package com.example.webmethodsanalyze;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.io.File;
import java.io.IOException;
import java.util.logging.*;

@SpringBootApplication
public class WebMethodsAnalyzeApplication {

    private static final Logger logger = Logger.getLogger(WebMethodsAnalyzeApplication.class.getName());

    static {
        try {
            // Create a file handler to log messages to a file
            FileHandler fileHandler = new FileHandler("app.log", false);
            fileHandler.setFormatter(new SimpleFormatter());

            // Create a console handler to display logs in the console
            ConsoleHandler consoleHandler = new ConsoleHandler();

            // Add handlers to the logger
            logger.addHandler(fileHandler);
            logger.addHandler(consoleHandler);

            // Set log levels
            logger.setLevel(Level.ALL);
            fileHandler.setLevel(Level.ALL);
            consoleHandler.setLevel(Level.ALL);

        } catch (IOException e) {
            logger.log(Level.SEVERE, "Failed to initialize file handler for logger", e);
        }
    }

    public static void main(String[] args) {
        SpringApplication.run(WebMethodsAnalyzeApplication.class, args);
    }

    @Bean
    public CommandLineRunner run(ApplicationContext ctx) {
        return args -> {
            PackageStructureVerifier verifier = ctx.getBean(PackageStructureVerifier.class);
            VariableLogger variableLogger = new VariableLogger();
            PackageStructureLogger packageStructureLogger = new PackageStructureLogger();

            File zipFile = new File("D:\\EDD_SRVSAP_eOrder_V1_0_copy.zip");

            try {
                verifier.verifyPackage(zipFile);
                PackageNode root = verifier.getParsedPackageNode(zipFile);
                packageStructureLogger.logStructure(root);
                variableLogger.logVariables(root);
            } catch (IOException e) {
                e.printStackTrace();
            }
        };
    }
}
