package com.example.webmethodsanalyze;

import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipInputStream;

@Component
public class FileService {

    public NodeType checkFileType(InputStream inputStream) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(inputStream);

            doc.getDocumentElement().normalize();
            Node node = doc.getDocumentElement();
            return mapType(traverse(node));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return NodeType.UNKNOWN;
    }

    private String traverse(Node node) {
        if (node != null && node.getNodeType() == Node.ELEMENT_NODE) {
            if (node.getNodeName().equals("Values")) {
                Node node1 = node.getFirstChild() != null ? node.getFirstChild().getNextSibling() : null;
                if (node1 != null) {
                    if (node1.getNodeName().equals("record")) {
                        Node textNode = node1.getFirstChild() != null ? node1.getFirstChild().getNextSibling() : null;
                        return textNode != null ? textNode.getTextContent() : "unknown";
                    }
                    return node1.getTextContent();
                }
            }
        }
        return "unknown";
    }

    private NodeType mapType(String type) {
        switch (type) {
            case "webMethods/trigger":
                return NodeType.TRIGGER;
            case "record":
                return NodeType.DOCUMENT_TYPE;
            case "flow":
                return NodeType.FLOW;
            case "java":
                return NodeType.JAVA_SERVICE;
            case "interface":
                return NodeType.FOLDER;
            case "ConnectionData":
                return NodeType.CONNECTION_DATA;
            case "AdapterService":
                return NodeType.ADAPTER_SERVICE;
            default:
                return NodeType.UNKNOWN;
        }
    }

    public NodeType checkFileTypeFromStream(ZipInputStream zis) {
        try {
            InputStream bais = StreamUtil.toByteArrayInputStream(zis);
            return checkFileType(bais);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return NodeType.UNKNOWN;
    }
}


