package com.example.webmethodsanalyze;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class VariablesAndMethods {

    public static List<String> verifyVariableNames(InputStream inputStream) {
        List<String> variableNames = new ArrayList<>();
        try {
            InputStream bais = StreamUtil.toByteArrayInputStream(inputStream);
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(bais);

            doc.getDocumentElement().normalize();
            Node node = doc.getDocumentElement();
            traverseAndCheckVariables(node, variableNames);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return variableNames;
    }

    private static void traverseAndCheckVariables(Node xmlFile, List<String> variableNames) {
        if (xmlFile.getNodeType() == Node.ELEMENT_NODE) {
            if (xmlFile.getNodeName().equals("value") && xmlFile.getParentNode().getNodeName().equals("record") && xmlFile.getParentNode().getParentNode().getNodeName().equals("array")) {
                NamedNodeMap map = xmlFile.getAttributes();
                Node node1 = map.item(0);
                if (node1.getNodeValue().equals("field_name")) {
                    variableNames.add(xmlFile.getTextContent());
                }
            }
            NodeList children = xmlFile.getChildNodes();
            for (int i = 0; i < children.getLength(); i++) {
                traverseAndCheckVariables(children.item(i), variableNames);
            }
        }
    }
}
