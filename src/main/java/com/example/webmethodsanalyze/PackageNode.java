package com.example.webmethodsanalyze;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class PackageNode {
    private String name;
    private NodeType type;
    private List<PackageNode> children;
    private PackageNode parent;
    private InputStream inputStream;

    public PackageNode(String name, NodeType type) {
        this.name = name;
        this.type = type;
        this.children = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public NodeType getType() {
        return type;
    }

    public List<PackageNode> getChildren() {
        return children;
    }

    public void setType(NodeType type) {
        this.type = type;
    }

    public void addChild(PackageNode child) {
        child.setParent(this);
        this.children.add(child);
    }

    private void setParent(PackageNode parent) {
        this.parent = parent;
    }

    public PackageNode getParent() {
        return parent;
    }

    public String getPath() {
        if (parent == null) {
            return name;
        } else {
            return parent.getPath() + "/" + name;
        }
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    @Override
    public String toString() {
        return "PackageNode{" +
                "name='" + name + '\'' +
                ", type=" + type +
                ", children=" + children +
                '}';
    }
}

enum NodeType {
    PACKAGE, FILE,FOLDER, FLOW, JAVA_SERVICE, CONNECTION_DATA, ADAPTER_SERVICE, TRIGGER, DOCUMENT_TYPE, UNKNOWN
}
