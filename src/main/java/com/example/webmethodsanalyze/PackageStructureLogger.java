package com.example.webmethodsanalyze;

import java.util.logging.Logger;

public class PackageStructureLogger {

    private static final Logger logger = Logger.getLogger(PackageStructureLogger.class.getName());

    public void logStructure(PackageNode node) {
        String validationMessage = NamingConventionVerifier.validateNodeName(node, node.getParent() != null ? node.getParent().getName() : node.getName());
        if (validationMessage.startsWith("Invalid")) {
            logger.info( validationMessage);
        }
        for (PackageNode child : node.getChildren()) {
            logStructure(child);
        }
    }
}
