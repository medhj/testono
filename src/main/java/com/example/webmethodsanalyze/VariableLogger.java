package com.example.webmethodsanalyze;

import java.util.List;
import java.util.logging.Logger;

public class VariableLogger {

    private static final Logger logger = Logger.getLogger(VariableLogger.class.getName());

    public void logVariables(PackageNode node) {
        if (node.getType() == NodeType.FLOW && node.getInputStream() != null) {
            List<String> variableNames = VariablesAndMethods.verifyVariableNames(node.getInputStream());
            for (String variableName : variableNames) {
                String validationMessage = NamingConventionVerifier.verifyVariableName(variableName);
                if (!validationMessage.startsWith("Valid")) {
                    logger.warning("variable: " + variableName + " - " + validationMessage + " --serviceName-- : " + node.getName());
                }
            }
        }

        for (PackageNode child : node.getChildren()) {
            logVariables(child);
        }
    }
}
