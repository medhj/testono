package com.example.webmethodsanalyze;

import org.springframework.stereotype.Component;

import java.util.List;


public class PackageStructurePrinter {

    public void printStructure(PackageNode node, String indent) {
        String validationMessage = NamingConventionVerifier.validateNodeName(node, node.getParent() != null ? node.getParent().getName() : node.getName());
        System.out.println(indent + node.getName() + " (" + node.getType() + ") - " + validationMessage);
        for (PackageNode child : node.getChildren()) {
            printStructure(child, indent + "  ");
        }
    }
    public void printAndValidateStructure(PackageNode node, String packageName) {


        if (node.getType() == NodeType.FLOW) {
            validateFlowServiceVariables(node);
        }

        for (PackageNode child : node.getChildren()) {
            printAndValidateStructure(child, packageName);
        }
    }

    private void validateFlowServiceVariables(PackageNode node) {
        if (node.getInputStream() != null) {
            List<String> variableNames = VariablesAndMethods.verifyVariableNames(node.getInputStream());
            for (String variableName : variableNames) {
                String validationMessage = NamingConventionVerifier.verifyVariableName(variableName);
                System.out.println("Variable: " + variableName + " - " + validationMessage +" --serviceName-- : "  + node.getName());
            }
        }
    }
}